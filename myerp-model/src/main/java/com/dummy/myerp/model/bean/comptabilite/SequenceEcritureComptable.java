package com.dummy.myerp.model.bean.comptabilite;


/**
 * Bean représentant une séquence pour les références d'écriture comptable
 */
public class SequenceEcritureComptable {

    // ==================== Attributs ====================
    /** L'année */
    private Integer annee;
    /** La dernière valeur utilisée */
    private Integer derniereValeur;

    // ==================== Constructeurs ====================
    /**
     * Constructeur
     */
    public SequenceEcritureComptable() {
    }

    public SequenceEcritureComptable(final Integer annee, final Integer derniereValeur) {
        this.annee = annee;
        this.derniereValeur = derniereValeur;
    }

    // ==================== Getters/Setters ====================
    public Integer getAnnee() {
        return annee;
    }
    public void setAnnee(Integer pAnnee) {
        annee = pAnnee;
    }
    public Integer getDerniereValeur() {
        return derniereValeur;
    }
    public void setDerniereValeur(Integer pDerniereValeur) {
        derniereValeur = pDerniereValeur;
    }

// ==================== Méthodes ====================

    @Override
    public String toString() {
        return "SequenceEcritureComptable{" +
                ", annee=" + annee +
                ", derniereValeur=" + derniereValeur +
                '}';
    }
}
