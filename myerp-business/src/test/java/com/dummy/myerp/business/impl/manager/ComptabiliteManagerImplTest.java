package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.contrat.BusinessProxy;
import com.dummy.myerp.business.impl.AbstractBusinessManager;
import com.dummy.myerp.business.impl.TransactionManager;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ComptabiliteManagerImplTest {

    ComptabiliteManagerImpl comptabiliteManagerTest;
    EcritureComptable ecritureComptable;

    private DaoProxy daoProxy;

    @BeforeEach
    public void init() {
        comptabiliteManagerTest = new ComptabiliteManagerImpl();
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setId(1);
        ecritureComptable.setJournal(new JournalComptable("BQ", "Achat"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Libelle");
        ecritureComptable.setReference("BQ-2020/00001");
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "200.50", null));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", "33"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "301"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, "40", "7"));
    }

    public void initMock() {
        daoProxy = mock(DaoProxy.class, Mockito.RETURNS_DEEP_STUBS);
        BusinessProxy mockBusinessProxy = mock(BusinessProxy.class, RETURNS_DEEP_STUBS);
        TransactionManager mockTransactionManager = mock(TransactionManager.class, RETURNS_DEEP_STUBS);
        AbstractBusinessManager.configure(mockBusinessProxy, daoProxy, mockTransactionManager);
    }

    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit);
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal(pCredit);
        String vLibelle = ObjectUtils.defaultIfNull(vDebit, BigDecimal.ZERO)
                .subtract(ObjectUtils.defaultIfNull(vCredit, BigDecimal.ZERO)).toPlainString();
        return new LigneEcritureComptable(new CompteComptable(pCompteComptableNumero),
                vLibelle,
                vDebit, vCredit);
    }

    @Test
    @Tag("GetterTest")
    void getListCompteComptable() {
        initMock();

        comptabiliteManagerTest.getListCompteComptable();
        then(daoProxy).should(times(1)).getComptabiliteDao();
    }

    @Test
    @Tag("GetterTest")
    void getListJournalComptable() {
        initMock();

        comptabiliteManagerTest.getListJournalComptable();
        then(daoProxy).should(times(1)).getComptabiliteDao();
    }

    @Test
    @Tag("GetterTest")
    void getListEcritureComptable() {
        initMock();

        comptabiliteManagerTest.getListEcritureComptable();
        then(daoProxy).should(times(1)).getComptabiliteDao();
    }

    @Test
    void insertEcritureComptable() throws FunctionalException, NotFoundException {
        initMock();

        EcritureComptable ecritureComptableReturn = new EcritureComptable();
        ecritureComptableReturn.setReference("BQ-2020/00001");
        ecritureComptableReturn.setId(1);

        when(daoProxy.getComptabiliteDao().getEcritureComptableByRef(anyString())).thenReturn(ecritureComptableReturn);

        comptabiliteManagerTest.insertEcritureComptable(ecritureComptable);
        then(daoProxy).should(atLeast(1)).getComptabiliteDao();
    }

    @Test
    void deleteEcritureComptable(){
        initMock();

        comptabiliteManagerTest.deleteEcritureComptable(ecritureComptable.getId());
        then(daoProxy).should(times(1)).getComptabiliteDao();
    }

    @Test
    void updateEcritureComptable() throws FunctionalException, NotFoundException {
        initMock();

        EcritureComptable ecritureComptableReturn = new EcritureComptable();
        ecritureComptableReturn.setReference("BQ-2020/00001");
        ecritureComptableReturn.setId(1);

        when(daoProxy.getComptabiliteDao().getEcritureComptableByRef(anyString())).thenReturn(ecritureComptableReturn);

        comptabiliteManagerTest.updateEcritureComptable(ecritureComptable);
        then(daoProxy).should(atLeast(1)).getComptabiliteDao();
    }

    @Test
    public void givenEcritureComptableComplete_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException () throws FunctionalException {

        comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable);
    }

    @Test
    public void givenEcritureComptableWithoutDate_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.setDate(null);

        FunctionalException exception =
                assertThrows(FunctionalException.class, ()-> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @Test
    public void givenEcritureComptableWithoutJournalCode_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.setJournal(null);

        FunctionalException exception =
                assertThrows(FunctionalException.class, ()-> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @Test
    public void givenEcritureComptableWithoutLibelle_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.setLibelle(null);

        FunctionalException exception =
                assertThrows(FunctionalException.class, ()-> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @Test
    public void givenEcritureComptableWithoutGoodReference_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException() {
        String arg = "BQs-2020/00001";
        ecritureComptable.setReference(arg);

        FunctionalException exception =
                assertThrows( FunctionalException.class, () -> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @Test
    public void givenEcritureComptableWithoutGoodMontant_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1,"222.123",null));

        FunctionalException exception =
                assertThrows( FunctionalException.class, () -> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @Test
    @Tag("RG2")
    public void givenEcritureComptableNotEquilibree_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1,"222",null));

        FunctionalException exception =
                assertThrows( FunctionalException.class, () -> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable n'est pas équilibrée.");
    }

    @Test
    @Tag("RG3")
    public void givenEcritureComptableWithoutTwoLigneEcriture_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1,"222", null));

        FunctionalException exception =
                assertThrows( FunctionalException.class, () -> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable ne respecte pas les règles de gestion.");

    }

    @Test
    @Tag("RG3")
    public void givenEcritureComptableWithTwoDebitEcriture_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1,"222", null));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1,"222", null));

        FunctionalException exception =
                assertThrows(FunctionalException.class, ()-> comptabiliteManagerTest.checkEcritureComptableUnit((ecritureComptable)));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable n'est pas équilibrée.");
    }

    @Test
    @Tag("RG3")
    public void givenEcritureComptableWithTwoCreditEcriture_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException(){
        ecritureComptable.getListLigneEcriture().clear();
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1,null, "222"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1,null, "222"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, ()-> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("L'écriture comptable n'est pas équilibrée.");
    }

    @Test
    @Tag("RG5")
    public void givenEcritureComptableWithWrongYear_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException() {
        ecritureComptable.setReference("01-"+ (Calendar.getInstance().get(Calendar.YEAR)-1) +"/00001");

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> comptabiliteManagerTest.checkEcritureComptableUnit(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("La référence de l'écriture doit contenir l'année en cours.");
    }

    @Test
    @Tag("RG6")
    public void givenEcritureComptableWithAlreadyUsedReference_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException() throws NotFoundException {
        initMock();

        EcritureComptable ecritureComptableReturn = new EcritureComptable();
        ecritureComptableReturn.setId(ecritureComptable.getId()+1);
        ecritureComptableReturn.setReference(ecritureComptable.getReference());

        when(daoProxy.getComptabiliteDao().getEcritureComptableByRef("BQ-2020/00001"))
                .thenReturn(ecritureComptableReturn);

        FunctionalException exception =
                assertThrows(FunctionalException.class, ()-> comptabiliteManagerTest.checkEcritureComptableContext(ecritureComptable));

        assertThat(exception.getMessage()).isEqualTo("Une autre écriture comptable existe déjà avec la même référence.");
    }

    @Test
    @Tag("RG6")
    public void givenEcritureComptableAlreadyExistInDB_whenCheckEcritureComptableUnit_thenAssertThrowsFunctionnalException() throws NotFoundException, FunctionalException {
        initMock();

        EcritureComptable ecritureComptableReturn = new EcritureComptable();
        ecritureComptableReturn.setId(1);
        ecritureComptableReturn.setReference("BQ-2020/00001");

        when(daoProxy.getComptabiliteDao().getEcritureComptableByRef(anyString())).thenReturn(ecritureComptableReturn);

        comptabiliteManagerTest.checkEcritureComptableContext(ecritureComptable);
    }

    @Test
    @Tag("RG6")
    public void givenEcritureComptableWithNewReference_whenCheckEcritureComptable_theAssertThrowsFunctionnalException() throws NotFoundException, FunctionalException {
        initMock();

        when(daoProxy.getComptabiliteDao().getEcritureComptableByRef(anyString()))
                .thenThrow(NotFoundException.class);

        comptabiliteManagerTest.checkEcritureComptableContext(ecritureComptable);
    }

    @Test
    @Tag("RG")
    public void givenCorrectEcritureComptable_whenCheckEcritureComptable_thenAssertThrowsFunctionnalException() throws NotFoundException, FunctionalException {
        initMock();

        when(daoProxy.getComptabiliteDao().getEcritureComptableByRef(anyString()))
                .thenThrow(NotFoundException.class);

        comptabiliteManagerTest.checkEcritureComptableContext(ecritureComptable);
    }

    @Test
    @Tag("ContextReference")
    public void givenFirstEcritureComptableOfYear_whenAddReference_thenAddFirstReferenceAndSaveFirstInSequenceEcritureComptable() throws NotFoundException {
        initMock();

        when(daoProxy.getComptabiliteDao().getSequenceEcritureComptableByYear("BQ",2020))
                .thenReturn(null);

        comptabiliteManagerTest.addReference(ecritureComptable);
        String [] reference = ecritureComptable.getReference().split("[-/]");
        String numberSequence = reference[2];

        assertThat(numberSequence).isEqualTo("00001");
        then(daoProxy).should(atLeast(2)).getComptabiliteDao();
    }

    @Test
    @Tag("ContextReference")
    public void givenXEcritureComptableOfYear_whenAddReference_thenAddNumberXPlusOneReferenceAndSaveNewNumberInSequenceEcritureComptable() throws NotFoundException {
        initMock();

        when(daoProxy.getComptabiliteDao().getSequenceEcritureComptableByYear("BQ",2020))
                .thenReturn(new SequenceEcritureComptable(2020,10));

        comptabiliteManagerTest.addReference(ecritureComptable);
        String[] reference = ecritureComptable.getReference().split("[-/]");
        String numberSequence = reference[2];

        assertThat(numberSequence).isEqualTo("00011");
        then(daoProxy).should(atLeast(2)).getComptabiliteDao();
    }
}