package com.dummy.myerp.testbusiness.business;

import com.dummy.myerp.business.impl.manager.ComptabiliteManagerImpl;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable;
import com.dummy.myerp.technical.exception.FunctionalException;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Transactional(propagation = Propagation.REQUIRES_NEW)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = "/com/dummy/myerp/business/applicationContext.xml")
//@Sql(scripts = {"classpath:/com/dummy/myerp/business/01_create_schema.sql",
//                "classpath:/com/dummy/myerp/business/02_create_tables.sql",
//                "classpath:/com/dummy/myerp/business/21_insert_data_demo.sql"})
public class ComptabiliteManagerImplIntegrationTest {

    ComptabiliteManagerImpl comptabiliteManagerTest;
    EcritureComptable ecritureComptable;

    @BeforeEach
    public void init(){
        comptabiliteManagerTest = new ComptabiliteManagerImpl();
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Libelle");
        ecritureComptable.setReference("BQ-2020/00001");
        ecritureComptable.getListLigneEcriture().add(this.createLigne(401, "200.50", null));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(401, "100.50", "33"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(411, null, "301"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(411, "40", "7"));
    }

    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit);
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal(pCredit);
        String vLibelle = ObjectUtils.defaultIfNull(vDebit, BigDecimal.ZERO)
                .subtract(ObjectUtils.defaultIfNull(vCredit, BigDecimal.ZERO)).toPlainString();
        return new LigneEcritureComptable(new CompteComptable(pCompteComptableNumero),
                vLibelle,
                vDebit, vCredit);
    }

    @Test
    final void getListCompteComptable() {
        List<CompteComptable> compteComptables;

        compteComptables = comptabiliteManagerTest.getListCompteComptable();

        assertThat(compteComptables.size()).isEqualTo(7);
    }

    @Test
    void getListJournalComptable() {
        List<JournalComptable> journalComptables;

        journalComptables = comptabiliteManagerTest.getListJournalComptable();

        assertThat(journalComptables.size()).isEqualTo(4);
    }

    @Test
    void getListInsertUpdateDelete_ecritureComptable() throws FunctionalException {

        List<EcritureComptable> ecritureComptables;
        EcritureComptable ecritureComptableDB;

        // ===== Get
        // WHEN
        ecritureComptables = comptabiliteManagerTest.getListEcritureComptable();
        // THEN
        assertThat(ecritureComptables.size()).isEqualTo(5);

        // ===== Insert
        //WHEN
        comptabiliteManagerTest.insertEcritureComptable(ecritureComptable);
        ecritureComptables = comptabiliteManagerTest.getListEcritureComptable();
        // THEN
        assertThat(ecritureComptables.size()).isEqualTo(6);

        // ===== Update
        //WHEN
        ecritureComptableDB = ecritureComptables.get(0);
        String libelle = ecritureComptableDB.getLibelle();
        String newLibelle = "newLibelle";
        int ecritureComptableDBId = ecritureComptableDB.getId();
        ecritureComptableDB.setLibelle(newLibelle);
        ecritureComptableDB.setJournal(new JournalComptable("BQ","Banque"));
        ecritureComptableDB.setReference("BQ-2020/00011");
        comptabiliteManagerTest.updateEcritureComptable(ecritureComptableDB);
        ecritureComptables = comptabiliteManagerTest.getListEcritureComptable();
        for ( EcritureComptable ecriture : ecritureComptables) {
            if (ecriture.getId() == ecritureComptableDBId)
                ecritureComptableDB = ecriture;
        }
        //THEN
        assertThat(libelle).isNotEqualTo(newLibelle);
        assertThat(ecritureComptableDB.getLibelle()).isEqualTo(newLibelle);
        assertThat(ecritureComptableDB.getReference()).isEqualTo("BQ-2020/00011");
        assertThat(ecritureComptables.size()).isEqualTo(6);

        // ===== Delete
        // WHEN
        comptabiliteManagerTest.deleteEcritureComptable(ecritureComptableDBId);
        ecritureComptables = comptabiliteManagerTest.getListEcritureComptable();
        EcritureComptable deleteEcritureComptable = null;
        for (EcritureComptable ecriture : ecritureComptables){
            if (ecriture.getId() == ecritureComptableDBId)
                deleteEcritureComptable = ecriture;
        }
        //THEN
        assertThat(ecritureComptables.size()).isEqualTo(5);
        assertThat(deleteEcritureComptable).isEqualTo(null);
    }

    @Test
    public void addReferenceIsFirstEcritureComptableOfYear(){
        ecritureComptable.setReference(null);

        comptabiliteManagerTest.addReference(ecritureComptable);

        assertThat(ecritureComptable.getReference()).isEqualTo("BQ-2020/00001");
    }

//    @Test
//    public void addReferenceEcritureComptable() {
//        ecritureComptable.setReference(null);
//        ecritureComptable.setJournal(new JournalComptable("AC","Achat"));
//
//        comptabiliteManagerTest.addReference(ecritureComptable);
//
//        assertThat(ecritureComptable.getReference()).isEqualTo("AC-2020/00041");
//    }
}
